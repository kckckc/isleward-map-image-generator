import * as fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';
import { setClientConfig } from '../src/clientConfig.js';
import { MapData, renderMap } from '../src/renderMap.js';

// Options for renderMap
const OPTIONS = {
	applyFlips: true,
	applyAlpha: true,
}

// Path for created images
const outpath = (f: string) => {
	return path.join(path.dirname(fileURLToPath(import.meta.url)), `../output/${f.replace('.json', '')}-map.png`);
}

// Load pre-downloaded clientConfig
setClientConfig(JSON.parse(fs.readFileSync('./data/clientConfig.json').toString()))

// Use pre-downloaded data files
const files = fs.readdirSync('./data');
files
	.filter(d => d !== 'clientConfig.json')
	.forEach(f => {
		try {
			const data: MapData = JSON.parse(fs.readFileSync(`./data/${f}`).toString());
			renderMap(data, outpath(f), OPTIONS);
		} catch (e) {
			console.log(`${f} failed`);
			console.log(e);
		}
	});
