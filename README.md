# Isleward map image generator

Generates Isleward map images from spritesheets, map data, and clientConfig

## Todo

* Handle spritesheet downloading (from iwd server automatically?)
* Handle hiddenRooms/interior hiders

## Usage

Programmatic: `setClientConfig(clientConfig)`, `renderMap(dataFromOnGetMap, outpath, options)`

Manually getting data:
- Download spritesheets
- `addons.events.on('onGetMap', (e) => console.log(JSON.stringify(e)))` then enter map
- `require(['js/system/globals'], (g) => {console.log(JSON.stringify(g.clientConfig))})`

Run: `yarn && yarn dev`

## Misc

* Types generated with [json2ts](http://json2ts.com/) (neat!)
