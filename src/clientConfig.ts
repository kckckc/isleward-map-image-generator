export interface Dimensions {
	w: number;
	h: number;
}

export interface AtlasTextureDimensions {
	[key: string]: Dimensions;
}

export interface SpriteSizes {
	[key: string]: number;
}

export interface SheetOpacityConfig {
	default: number;
	max?: number;
	[key: number]: number;
}

export interface TileOpacities {
	[key: string]: SheetOpacityConfig;
}

export interface TilesNoFlip {
	[key: string]: number[];
}

export interface Player {
	text: string;
	module: string;
	method: string;
	threadModule: string;
}

export interface ContextMenuActions {
	player: Player[];
	npc: any[];
}

export interface ClientComponent {
	type: string;
	path: string;
}

export interface Ui {
	name: string;
	file: string;
}

export interface Sounds {
	ui: Ui[];
}

export interface Tos {
	version: string;
	required: boolean;
	content: string;
}

export interface ClientConfig {
	logoPath: string;
	loginBgGeneratorPath: string;
	resourceList: string[];
	textureList: string[];
	bigTextures: string[];
	atlasTextureDimensions: AtlasTextureDimensions;
	atlasTextures: string[];
	spriteSizes: SpriteSizes;
	blockingTileIndices: number[];
	tileOpacities: TileOpacities;
	tilesNoFlip: TilesNoFlip;
	uiLoginList: any[];
	uiList: any[];
	contextMenuActions: ContextMenuActions;
	clientComponents: ClientComponent[];
	sounds: Sounds;
	tos: Tos;
}

let clientConfig: ClientConfig | null = null;
export const setClientConfig = (data: ClientConfig) => {
	clientConfig = data;
}

class MissingClientConfigError extends Error {
	constructor(message: string) {
		super(message);

		this.name = this.constructor.name;

		Error.captureStackTrace(this, this.constructor);
	}
}

export const getSheetNum = (tile: number) => {
	if (tile < 224) return 0;
	else if (tile < 480) return 1;
	return 2;
}

export const getSheetName = (tile: number) => {
	if (clientConfig === null) {
		throw new MissingClientConfigError('Client config must be provided first');
	}

	const { atlasTextures } = clientConfig;

	const sheetNum = getSheetNum(tile);
	const sheetName = atlasTextures[sheetNum];

	return sheetName;
}

export const getOffsetAndSheet = (tile: number) => {
	if (clientConfig === null) {
		throw new MissingClientConfigError('Client config must be provided first');
	}

	const { atlasTextures, atlasTextureDimensions } = clientConfig;

	let offset = 0;
	let sheetName = null;

	let aLen = atlasTextures.length;
	for (let i = 0; i < aLen; i++) {
		sheetName = atlasTextures[i];

		const dimensions = atlasTextureDimensions[sheetName];
		const spriteCount = dimensions.w * dimensions.h;

		if (offset + spriteCount > tile)
			break;

		offset += spriteCount;
	}

	return { offset, sheetName }
}

export const mapOpacity = (tile: number) => {
	if (clientConfig === null) {
		throw new MissingClientConfigError('Client config must be provided first');
	}

	const { tileOpacities } = clientConfig;

	const { offset, sheetName } = getOffsetAndSheet(tile);
	const mappedTile = tile - offset;

	const opacityConfig = (tileOpacities[sheetName!]) || tileOpacities.default;

	let alpha = (opacityConfig[mappedTile] || opacityConfig.default);
	if (opacityConfig.max !== null) {
		alpha = alpha + (Math.random() * (alpha * 0.2));
		alpha = Math.min(1, alpha);
	}

	return alpha;
}

export const canFlip = (tile: number) => {
	if (clientConfig === null) {
		throw new MissingClientConfigError('Client config must be provided first');
	}

	const { tilesNoFlip } = clientConfig;

	const { offset, sheetName } = getOffsetAndSheet(tile);
	const mappedTile = tile - offset;

	const noFlipTiles = tilesNoFlip[sheetName!];
	if (!noFlipTiles)
		return true;

	return !noFlipTiles.includes(mappedTile);
}

export const isBigTexture = (sheetName: string) => {
	if (clientConfig === null) {
		throw new MissingClientConfigError('Client config must be provided first');
	}

	return clientConfig.bigTextures.includes(sheetName);
}
