import Jimp from 'jimp';
import { getOffsetAndSheet, isBigTexture } from './clientConfig.js';

const sheetCache: Record<string, Jimp> = {};
const spriteCache: Record<number, Jimp> = {};

// Internal sheet loader
// For now, sheets have to be manually downloaded and updated
const loadSheet = (sheetName: string) => {
	return new Promise<Jimp>((resolve) => {
		if (!sheetName.endsWith('.png')) sheetName += '.png';

		Jimp.read(`./spritesheets/${sheetName}`).then(resolve);
	});
}

// Retrieve spritesheet from cache or load if needed
const getSheet = async (sheetName: string) => {
	// Prefix for modded spritesheets
	sheetName = sheetName.replace('server/mods/iwd-fjolgard/images/', 'iwd-fjolgard-');

	let cached = sheetCache[sheetName];
	if (!cached) {
		cached = await loadSheet(sheetName);
		sheetCache[sheetName] = cached;
	}
	return cached;
}

// Retrieve tile texture from cache or create if needed
export const getTexture = async (c: number) => {
	if (spriteCache[c]) {
		return spriteCache[c];
	}

	const { offset, sheetName } = getOffsetAndSheet(c);
	const sheet = await getSheet(sheetName!);

	// c is now the cell index in the specific sheet, not the concatenated sheet that iwd generates
	c -= offset;

	// Convert cell index to x and y
	const y = Math.floor(c / 8);
	const x = c - (y * 8);

	const isBig = isBigTexture(sheetName!);
	const size = isBig ? 24 : 8;

	// Clone spritesheet and crop to desired sprite
	const spriteImage = sheet.clone();
	spriteImage.crop(x * size, y * size, size, size);

	// Fix offset to cache!!! by adding back the offset we subtracted
	spriteCache[c + offset] = spriteImage;
	return spriteImage;
}
