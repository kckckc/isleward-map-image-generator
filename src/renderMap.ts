import { canFlip, mapOpacity } from './clientConfig.js';
import mkdirp from 'mkdirp';
import Jimp from 'jimp';
import { getTexture } from './tileTextures.js';
import path from 'path';

export interface ClientObject {
	sheetName: string;
	cell?: number;
	name: string;
	x?: number;
	y?: number;
	width: number;
	height: number;
	zoneId: number;
	area: number[][];
}

export interface HiddenRoomProperties {
	fog: string;
	interior: string;
}

export interface HiddenRoom {
	clientObj: boolean;
	sheetName?: any;
	cell?: any;
	x: number;
	y: number;
	name: string;
	properties: HiddenRoomProperties;
	layerName: string;
	width: number;
	height: number;
	area: number[][];
	fog: string;
	interior: string;
	layer: number;
}

export interface MapData {
	zoneId: number;
	map: (string[] | number | string)[][];
	collisionMap: number[][];
	clientObjects: ClientObject[];
	hiddenRooms: HiddenRoom[];
}

export interface IRenderMapOptions {
	applyFlips?: boolean,
	applyAlpha?: boolean,
}

const BACKGROUND_COLOR = Jimp.cssColorToHex('#2D2136');

const createImage = async (w: number, h: number) => {
	return new Promise<Jimp>((resolve, reject) => {
		new Jimp(8 * w, 8 * h, BACKGROUND_COLOR, (err, image) => {
			if (err) {
				reject(err)
			} else {
				resolve(image);
			}
		})
	});
}

export const renderMap = async function(data: MapData, outpath: string, options: IRenderMapOptions = {}) {
	const rawMap = data.map;
	const w = rawMap.length;
	const h = rawMap[0].length;

	await mkdirp(path.dirname(outpath));

	const image = await createImage(w, h);

	async function buildTile(c: number, i: number, j: number) {
		const tileOpacity = mapOpacity(c);
		const tileCanFlip = canFlip(c);

		let texture = await getTexture(c);

		if (!texture) {
			console.log('no texture?');
			return;
		};

		// const sheetName = getSheetName(c);
		// const isBig = isBigTexture(sheetName!);
		// const size = isBig ? 24 : 8;

		if (options.applyFlips && tileCanFlip && Math.random() > 0.5) {
			texture = texture.clone().flip(true, false);
		}

		if (options.applyAlpha) {
			texture = texture.clone().opacity(tileOpacity);
		}

		image.composite(texture, i * 8, j * 8);
	}

	const map = [];

	// parse format?
	for (let i = 0; i < w; i++) {
		let rawRow = rawMap[i];
		let newRow: number[][] = [];
		map.push(newRow);
		for (let j = 0; j < h; j++) {
			let cell = rawRow[j];

			if (Array.isArray(cell)) {
				newRow.push(cell.map(c => parseInt(c)));
				continue;
			}
			if (typeof cell === 'number') {
				newRow.push([cell]);
				continue;
			}
			if (typeof cell === 'string') {
				newRow.push(cell.split(',').map(c => parseInt(c)));
			}
		}
	}

	for (let i = 0; i < w; i++) {
		const mapRow = map[i];

		for (let j = 0; j < h; j++) {
			const cell = mapRow[j];
			if (!cell) continue;

			const cLen = cell.length;
			if (!cLen) continue;

			const isHidden = false;
			// TODO: hidden room handling
			// const isHidden = checkHidden(i, j);
			// if (isHidden) {
			// 	// Render fake
			// } else {

			// }

			for (let k = 0; k < cLen; k++) {
				let c = cell[k];
				if (c === 0) continue;
				if (isNaN(c)) continue;

				const isFake = +c < 0;
				if (isFake && !isHidden) continue;
				if (!isFake && isHidden) continue;

				if (isFake) c = -c;

				c--;

				let flipped = '';
				if (canFlip(c)) {
					if (Math.random() < 0.5) {
						flipped = 'flip';
					}
				}

				// Render tile
				await buildTile(c, i, j);
			}
		}
	}

	await image.writeAsync(outpath);
}
